# mabaya
##Overview  
1. First and important, I enjoyed doing this task. Spring boot is great!  
2. Two decisions I made which I'm not sure if this was your intention:  
       1. I needed a way to associate the products with a campaign (besides the category).
       I created an Entity called Customer. A product is associated with a Customer and a Campaign is also associated with a Customer.  
       2. You wrote in the task to return the product with the highest bid. The bid is a property of Campaign, I'm not sure if I missed something here but in my implementation I return a product from the campaign with the highest bid (both of course associated with provided category)
3. The product selection strategy is very naive - campaign with lower bid on the same category will never be served.
   This was the definition in the task, I did not implement something more sophisticated.      

##Prerequisites/assumptions:  
1. JDK 1.8  
2. Project was written on a Mac machine, it was not tested
   on windows machine.  

##To check the project:  
1. Start with git clone git@bitbucket.org:derazongolan/mabaya.git  
2. By default application.properties contains h2 DB properties. To run the service on MySql, copy application.properties.mysql to application.properties.   
3. Go to project root, run ./gradlew test  
4. run ./gradlew bootRun  

##APis:  
1. To populate database with random data: 
[POST] http://127.0.0.1:8080/mabaya/randomdata  
2. To create campaign:  
[POST] http://127.0.0.1:8080/mabaya/campaign   
API expects the following form params: name, category, customer, bid
(customer is customer name)  
3. To serve ad:  
[GET] http://127.0.0.1:8080/mabaya/ad?category=xxxyyy  

##ToDo:  
The project is a test project and as such, it is not expected to deal with all real live issues.  
The main two issues which I feel less comfortable about are:  
1. Error handling: currently, I use the spring built-in error handling.  
2. DB related issues. Much work can be done to improve work with DB:  
indexing, improve queries, improve constraints  etc'. It requires time. 