package mabaya.controller;

import com.google.gson.Gson;
import mabaya.model.*;
import mabaya.service.AdService;
import mabaya.service.FixtureService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Date;
import java.util.Calendar;
import java.util.Map;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ControllerTest {
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private MockMvc mvc;

    @Autowired
    private AdService adService;

    @Autowired
    private FixtureService fixtureService;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CampaignRepository campaignRepository;

    @Test
    public void createCampaign() throws Exception {

        MvcResult result = mvc.perform(post("/mabaya/campaign")
                .content("name=golan&category=stam&customer=topshop&bid=100")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED))
                .andExpect(status().isOk()).andReturn();
        String content = result.getResponse().getContentAsString();
        Gson gson = new Gson();
        Map map = gson.fromJson(content, Map.class);
        Assert.assertEquals("stam", map.get("category"));
        log.info("----------------------------");
        log.info(content);
        log.info("----------------------------");
    }

    @Test
    public void serveAd() throws Exception {
        fixtureService.populateWithRandomData();
        for (int i =0;i<1000;i++) {
            mvc.perform(get("/mabaya/ad?category=stam")).andExpect(status().isOk());
        }
    }

    @Test
    public void createOldCampaign() throws Exception {
        fixtureService.populateWithRandomData();
        Customer newCustomer = Customer.createCustomer("old");
        customerRepository.save(newCustomer);
        Product newProduct = Product.createProduct("CATT-001", "newProd", 100000, "newcategory", newCustomer);
        productRepository.save(newProduct);

        Campaign myCampaign = Campaign.createCampaign("valid", "newcategory", 100000, newCustomer);
        campaignRepository.save(myCampaign);
        Product prod = adService.serveAd("newcategory");
        Assert.assertNotNull(prod);
        Assert.assertEquals(prod.getName(), "newProd");

        myCampaign.setCreateDateTime(new Date(Calendar.getInstance().getTimeInMillis() - 15 * 24 * 60 * 60 * 1000));
        campaignRepository.save(myCampaign);

        prod = adService.serveAd("newcategory");

        //@todo this assertion fails every few runs, there is something not deterministic in the test/
        //      It requires some more work
        Assert.assertNotNull(prod);
        Assert.assertNotEquals("newProd",prod.getName());

    }

    @Test
    public void testRandomDataGeneration() throws Exception {
        fixtureService.populateWithRandomData();
    }
}

