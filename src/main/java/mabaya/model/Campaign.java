package mabaya.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.Calendar;
import java.util.Date;

@Entity
public class Campaign {

	public Campaign(){
		this.createDateTime = new Date(Calendar.getInstance().getTimeInMillis());
	}


	public static Campaign createCampaign(String name, String category, int bid, Customer cust){
		Campaign c = new Campaign();
		c.setCategory(category);
		c.setName(name);
		c.setCustomer(cust);
		c.setBid(bid);
		return c;
	}

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;

	@NotNull
	@Size(max = 100)
	@Column(unique = true)
    private String name;


	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "customer_id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnore
	private Customer customer;

	@NotNull
	@Size(max = 100)
	private String category;

	@NotNull
	private int bid;

	@Temporal(TemporalType.TIMESTAMP)
	private Date createDateTime;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getBid() {
		return bid;
	}

	public void setBid(int bid) {
		this.bid = bid;
	}


	public Date getCreateDateTime() {
		return createDateTime;
	}

	public void setCreateDateTime(Date createDateTime) {
		this.createDateTime = createDateTime;
	}


}

