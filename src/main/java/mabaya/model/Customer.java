package mabaya.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Customer {
	public Customer(){
	}

	public static Customer createCustomer(String name) {
		Customer c = new Customer();
		c.setName(name);
		return c;
	}

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer id;

	@NotNull
	@Size(max = 100)
	@Column(unique = true)
    private String name;


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

