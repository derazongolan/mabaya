package mabaya.model;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;


import java.util.Date;
import java.util.List;

public interface CampaignRepository extends CrudRepository<Campaign, Integer> {

    /**
     */
    @Query("SELECT c FROM Campaign c where c.category = ?1 and c.createDateTime > ?2 ORDER BY c.bid DESC")
    List<Campaign> findTopByCategoryOrderByBidDesc(String category, Date after);

    /**
     */
    @Query("SELECT c FROM Campaign c where c.createDateTime > ?1 ORDER BY c.bid DESC")
    List<Campaign> findTopOrderByBidDesc(Date after);
}
