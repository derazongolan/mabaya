package mabaya.model;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import java.util.List;

public interface ProductRepository extends CrudRepository<Product, Integer> {

    @Query("SELECT p FROM Product p where p.customer = ?1 and p.category = ?2 ORDER BY p.price DESC")
    List<Product> findFirstByCustomerOrderByPriceDesc(Customer customer,String category);
}
