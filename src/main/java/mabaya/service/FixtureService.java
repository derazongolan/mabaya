package mabaya.service;

import mabaya.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.Vector;

/**
 * Creates mock data for testing.
 */
@Service
public class FixtureService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private CampaignRepository campaignRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CustomerRepository customerRepository;

    public void populateWithRandomData() throws Exception {

        List<Customer> exists = customerRepository.findByName("customer_0");
        if (exists.size() > 0){
            log.info("Database already populated with random data. Returning.");
            return;
        }
        Random rand = new Random();
        Vector<String> categories = new Vector<>();
        for (int i =0; i< 100 ;i++){
            categories.addElement("category_"+i);
        }

        Vector<Customer> customers = new Vector<>();
        for (int i =0; i< 20 ;i++){
            Customer c = Customer.createCustomer("customer_" + i);
            customerRepository.save(c);
            customers.addElement(c);
        }

        for (int i =0; i< 2000 ;i++){
            int randomCategory = rand.nextInt(100);
            int randomCustomer = rand.nextInt(20);
            int randomPrice = rand.nextInt(1000);
            String catalogNumber = "CATR" + String.format("%04d", i);
            Product p = Product.createProduct(catalogNumber,
                    "product_"+i,
                    randomPrice,
                    categories.elementAt(randomCategory),
                    customers.elementAt(randomCustomer));
            productRepository.save(p);
        }

        for (int i =0; i< 500 ;i++){
            int randomCategory = rand.nextInt(100);
            int randomCustomer = rand.nextInt(20);
            int randomBid = rand.nextInt(500);
            Campaign c = Campaign.createCampaign("campaign_"+i,
                    categories.elementAt(randomCategory),randomBid,
                    customers.elementAt(randomCustomer));
            campaignRepository.save(c);
        }

    }

    public void populateWithInitialData() throws Exception {
        List<Customer> exists = customerRepository.findByName("h&m");
        if (exists.size() > 0){
            log.info("Database already populated with random data. Returning.");
            return;
        }

        Customer hnm = Customer.createCustomer("h&m");
        customerRepository.save(hnm);
        Customer topshop = Customer.createCustomer("topshop");
        customerRepository.save(topshop);
        productRepository.save(Product.createProduct("CATA-001","HnMKidsShoes", 10,"shoe",hnm));
        productRepository.save(Product.createProduct("CATA-002","HnAdultsShoes", 100,"shoe",hnm));

        productRepository.save(Product.createProduct("CATA-003","HnMKidsShorts", 10,"short",hnm));
        productRepository.save(Product.createProduct("CATA-004","HnAdultsShorts", 100,"short",hnm));


        productRepository.save(Product.createProduct("CATA-005","topshopKidsShoes", 10,"shoe",topshop));
        productRepository.save(Product.createProduct("CATA-006","topshopAdultsShoes", 100,"shoe",topshop));

        productRepository.save(Product.createProduct("CATA-007","topshopKidsShorts", 10,"short",topshop));
        productRepository.save(Product.createProduct("CATA-008","topshopAdultsShorts", 100,"short",topshop));

        campaignRepository.save(Campaign.createCampaign("hnm_shoes","shoe",100,hnm));
        campaignRepository.save(Campaign.createCampaign("top_shoes","shoe",110,topshop));
    }
}
