package mabaya.service;

import mabaya.model.Campaign;
import mabaya.model.CampaignRepository;
import mabaya.model.Customer;
import mabaya.model.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *  Business logic for dealing with Campaigns management.
 */
@Service
public class CampaignService {

    @Autowired
    private CampaignRepository campaignRepository;

    @Autowired
    private CustomerRepository customerRepository;

    public Campaign createCampaign(String campaignName, String category,int bid,String customerName) throws Exception {
        List<Customer> customers = customerRepository.findByName(customerName);
        if (customers.size() == 0) {
            throw new Exception("Could not find customer " + customerName);
        }
        Campaign n = Campaign.createCampaign(campaignName,category,bid,customers.get(0));
        campaignRepository.save(n);
        return n;
    }
}
