package mabaya.service;

import org.springframework.stereotype.Service;
import mabaya.model.Campaign;
import mabaya.model.CampaignRepository;
import mabaya.model.Product;
import mabaya.model.ProductRepository;

import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

/**
 *  Business logic for serving ads
 */
@Service
public class AdService {

    @Autowired
    private CampaignRepository campaignRepository;

    @Autowired
    private ProductRepository productRepository;

    /**
     * Given ad category returns a selected product to show to the user.
     */
    public Product serveAd(String category) throws Exception {
        Date tenDaysAgo = new Date(Calendar.getInstance().getTimeInMillis() - 10*24*60*60*1000 );
        Random rand = new Random();

        List<Campaign> campaigns = campaignRepository.findTopByCategoryOrderByBidDesc(category,tenDaysAgo);
        if (campaigns.size() == 0){
            campaigns = campaignRepository.findTopOrderByBidDesc(tenDaysAgo);
        }

        if (campaigns.size() == 0){
            return null;
        }
        //@todo there might be several campaigns with the same highest bid, I need to choose one of them.
        //      Currently choosing the first one.
        List<Product> products = productRepository.findFirstByCustomerOrderByPriceDesc(
                campaigns.get(0).getCustomer(),
                campaigns.get(0).getCategory());

        if (products.size() == 0){
            return null;
        }

        return products.get(rand.nextInt(products.size()));
    }
}
