package mabaya.controller;

import mabaya.model.Campaign;
import mabaya.model.Product;
import mabaya.service.AdService;
import mabaya.service.CampaignService;
import mabaya.service.FixtureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path="/mabaya")
public class MainController {

	@Autowired
	private CampaignService campaignService;

	@Autowired
	private AdService adService;

	@Autowired
	private FixtureService fixtureService;

	@PostMapping(path="randomdata")
	public ResponseEntity generateRandomData() throws Exception{
		fixtureService.populateWithRandomData();
		return ResponseEntity.ok(HttpStatus.OK);
	}

	@PostMapping(path="campaign", produces= MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object>add(@RequestParam String name,@RequestParam String category,
									 @RequestParam int bid,@RequestParam String customer) throws Exception{
		Campaign campaign = campaignService.createCampaign(name,category,bid,customer);
		return new ResponseEntity<>(campaign,HttpStatus.OK);
	}

	@GetMapping(path="ad", produces= MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object>serveAd(@RequestParam String category) throws Exception{
		Product product = adService.serveAd(category);
		return new ResponseEntity<>(product,HttpStatus.OK);
	}
}
